import { SortDirection } from '@swimlane/ngx-datatable';

export class CompareUtils {
  static getSortDirection(dir: SortDirection): number {
    return dir === SortDirection.asc ? 1 : -1;
  }

  static isNumeric(str: any): boolean {
    if (typeof str !== 'string') {
      return false;
    }
    return !isNaN(str as any) && !isNaN(parseFloat(str));
  }
}

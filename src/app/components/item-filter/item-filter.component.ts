import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Item } from 'src/app/models/item.model';

@Component({
  selector: 'app-item-filter',
  templateUrl: './item-filter.component.html',
  styleUrls: ['./item-filter.component.scss'],
})
export class ItemFilterComponent implements OnInit {
  @Output() filter = new EventEmitter();
  @Input() visibleFilter: Map<keyof Item, boolean> | undefined;
  public itemFilter: FormGroup | undefined;
  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.itemFilter = this.fb.group({
      title: [{ value: '', disabled: this.visibleFilter && !this.visibleFilter!.has('title') }],
      description: [{ value: '', disabled: this.visibleFilter && !this.visibleFilter!.has('description') }],
      price: [{ value: '', disabled: this.visibleFilter && !this.visibleFilter!.has('price') }],
      email: [{ value: '', disabled: this.visibleFilter && !this.visibleFilter!.has('email') }],
    });
  }

  onSubmit(): void {
    this.filter.emit(this.itemFilter!.value);
    this.itemFilter!.reset(this.itemFilter!.value);
  }

  hasFavoriteFlag(): boolean {
    return !this.visibleFilter || !!this.visibleFilter!.has('favorite');
  }
}

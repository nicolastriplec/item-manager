import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ItemFilterComponent } from './item-filter.component';

describe('ItemFilterComponent', () => {
  let component: ItemFilterComponent;
  let fixture: ComponentFixture<ItemFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ItemFilterComponent],
      imports: [FormsModule, ReactiveFormsModule, MatFormFieldModule, MatButtonModule, MatInputModule, BrowserAnimationsModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemFilterComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get visible fields', () => {
    component.visibleFilter = new Map([['title', true]]);
    fixture.detectChanges();
    expect(component.itemFilter?.get('title')?.disabled).toBe(false);
    expect(component.itemFilter?.get('price')?.disabled).toBe(true);
    expect(component.hasFavoriteFlag()).toBe(false);
  });

  it('should emit filter value', () => {
    fixture.detectChanges();
    const newFilterEntry = { title: 'a' };
    const emitFilterSpy = spyOn(component.filter, 'emit');
    component.itemFilter?.patchValue(newFilterEntry);
    component.onSubmit();
    expect(emitFilterSpy).toHaveBeenCalledWith({ description: '', price: '', email: '', ...newFilterEntry });
  });
});

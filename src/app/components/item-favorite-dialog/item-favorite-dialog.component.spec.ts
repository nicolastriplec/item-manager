import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ItemDatatableComponent } from '../item-datatable/item-datatable.component';
import { ItemFilterComponent } from '../item-filter/item-filter.component';
import { ItemManagerComponent } from '../item-manager/item-manager.component';
import { ItemFavoriteDialogComponent } from './item-favorite-dialog.component';

describe('ItemFavoriteDialogComponent', () => {
  let component: ItemFavoriteDialogComponent;
  let fixture: ComponentFixture<ItemFavoriteDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ItemFavoriteDialogComponent, ItemManagerComponent, ItemFilterComponent, ItemDatatableComponent],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        HttpClientModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatInputModule,
        NgxDatatableModule,
        MatIconModule,
        MatDialogModule,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemFavoriteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

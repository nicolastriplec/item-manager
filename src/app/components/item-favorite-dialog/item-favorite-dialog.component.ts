import { AfterViewInit, Component } from '@angular/core';
import { SelectionType } from '@swimlane/ngx-datatable';
import { of } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { delay, tap } from 'rxjs/operators';
import { Item } from 'src/app/models/item.model';

@Component({
  selector: 'app-item-favorite-dialog',
  templateUrl: './item-favorite-dialog.component.html',
  styleUrls: ['./item-favorite-dialog.component.scss'],
})
export class ItemFavoriteDialogComponent implements AfterViewInit {
  public visibleColumns: Observable<Array<keyof Item>> = of(['title', 'image', 'remove']);
  public visibleFilter: Map<keyof Item, boolean> = new Map([['title', true]]);
  public baseFilter: Item = { favorite: true };
  public selectionType = SelectionType.checkbox;
  constructor() {}

  /** [Bug fix ngx-datatable] incorrect resize on dialog open animation (visual): https://github.com/swimlane/ngx-datatable/issues/923 */
  ngAfterViewInit(): void {
    of(null)
      .pipe(
        delay(200),
        tap(_ => window.dispatchEvent(new Event('resize')))
      )
      .subscribe();
  }
}

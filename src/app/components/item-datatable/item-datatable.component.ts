import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { ColumnMode, SelectionType, TableColumn } from '@swimlane/ngx-datatable';
import { get, isArray } from 'lodash';
import { of } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';
import { Item } from 'src/app/models/item.model';
import { SortingEvent } from 'src/app/models/sorting-event.model';

@Component({
  selector: 'app-item-datatable',
  templateUrl: './item-datatable.component.html',
  styleUrls: ['./item-datatable.component.scss'],
})
export class ItemDatatableComponent implements OnInit, AfterViewInit {
  @ViewChild('imgTmpl', { static: true }) imgTmpl: TemplateRef<any> | undefined;
  @ViewChild('favTmpl', { static: true }) favTmpl: TemplateRef<any> | undefined;
  @ViewChild('removeTmpl', { static: true }) removeTmpl: TemplateRef<any> | undefined;
  @Input() rows: Array<Item> | null | undefined;
  @Input() selectionType = SelectionType.single;
  @Input() visibleColumns: Observable<Array<keyof Item>> | undefined;
  @Output() sort = new EventEmitter();
  @Output() selectItem = new EventEmitter();
  @Output() removeFavItem = new EventEmitter();
  public columns$: Observable<TableColumn[]> | undefined;
  public columnMode = ColumnMode.flex;

  constructor() {}

  ngOnInit(): void {
    const originalColumns: TableColumn[] = [
      {
        prop: 'favorite',
        width: 100,
        resizeable: false,
        canAutoResize: false,
        cellTemplate: this.favTmpl,
        sortable: false,
        cellClass: 'item-cell',
      },
      { prop: 'title', flexGrow: 1, cellClass: 'item-cell' },
      { prop: 'description', flexGrow: 1, cellClass: 'item-cell' },
      { prop: 'price', flexGrow: 1, cellClass: 'item-cell' },
      { prop: 'email', flexGrow: 1, cellClass: 'item-cell' },
      {
        prop: 'image',
        cellClass: 'item-cell',
        flexGrow: 1,
        cellTemplate: this.imgTmpl,
        sortable: false,
      },
      {
        prop: 'remove',
        width: 100,
        name: '',
        resizeable: false,
        canAutoResize: false,
        cellTemplate: this.removeTmpl,
        sortable: false,
        cellClass: 'item-cell',
      },
    ];

    this.columns$ = this.visibleColumns
      ? this.visibleColumns.pipe(map(visibleColumns => originalColumns!.filter(field => visibleColumns.includes(field.prop as keyof Item))))
      : of(originalColumns);
  }

  onSort(event: SortingEvent): void {
    this.sort.emit(get(event, `sorts[0]`));
  }

  onSelect(event: { selected: Array<Item> }): void {
    this.selectItem.emit(isArray(get(event, 'selected')) && get(event, 'selected[0]'));
  }

  onRemoveFavItem(item: Item): void {
    this.removeFavItem.emit(item);
  }

  /** [Bug fix ngx-datatable] columnMode.flex ngx-datatable resize issue: https://github.com/swimlane/ngx-datatable/issues/919 */
  ngAfterViewInit(): void {
    requestAnimationFrame(() => (this.columnMode = ColumnMode.force));
  }
}

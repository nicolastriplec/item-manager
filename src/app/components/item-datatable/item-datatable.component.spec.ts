import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatIconModule } from '@angular/material/icon';
import { NgxDatatableModule, SortDirection } from '@swimlane/ngx-datatable';
import { of } from 'rxjs';
import { delay, take } from 'rxjs/operators';
import { SortingEvent } from 'src/app/models/sorting-event.model';
import { ItemDatatableComponent } from './item-datatable.component';

describe('ItemDatatableComponent', () => {
  let component: ItemDatatableComponent;
  let fixture: ComponentFixture<ItemDatatableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ItemDatatableComponent],
      imports: [NgxDatatableModule, MatIconModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemDatatableComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get visible columns', done => {
    component.visibleColumns = of(['title']);
    fixture.detectChanges();
    component.columns$?.pipe(take(1)).subscribe(columns => {
      expect(columns).toHaveSize(1);
      done();
    });
  });

  it('should emit remove item', () => {
    fixture.detectChanges();
    const emitFilterSpy = spyOn(component.removeFavItem, 'emit');
    component.onRemoveFavItem({ title: 'test' });
    expect(emitFilterSpy).toHaveBeenCalledWith({ title: 'test' });
  });

  it('should emit select item', () => {
    fixture.detectChanges();
    const emitFilterSpy = spyOn(component.selectItem, 'emit');
    component.onSelect({ selected: [{ title: 'test' }] });
    expect(emitFilterSpy).toHaveBeenCalledWith({ title: 'test' });
  });

  it('should emit sort column', () => {
    fixture.detectChanges();
    const emitFilterSpy = spyOn(component.sort, 'emit');
    component.onSort({ sorts: [{ dir: SortDirection.asc, prop: 'title' }] } as SortingEvent);
    expect(emitFilterSpy).toHaveBeenCalledWith({ dir: SortDirection.asc, prop: 'title' });
  });
});

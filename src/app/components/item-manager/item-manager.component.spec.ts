import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxDatatableModule, SortDirection } from '@swimlane/ngx-datatable';
import { ItemManagerService } from 'src/app/services/item-manager.service';
import { ItemDatatableComponent } from '../item-datatable/item-datatable.component';
import { ItemFilterComponent } from '../item-filter/item-filter.component';
import { ItemManagerComponent } from './item-manager.component';

describe('ItemManagerComponent', () => {
  let component: ItemManagerComponent;
  let fixture: ComponentFixture<ItemManagerComponent>;
  let itemManagerService: ItemManagerService;
  const testId = 'testId';

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ItemManagerComponent, ItemFilterComponent, ItemDatatableComponent],
      imports: [FormsModule, ReactiveFormsModule, HttpClientModule, MatInputModule, MatButtonModule, BrowserAnimationsModule, NgxDatatableModule, MatIconModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemManagerComponent);
    component = fixture.componentInstance;
    component.id = testId;
    itemManagerService = TestBed.inject(ItemManagerService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get current data', () => {
    expect(component.rows$).toBeDefined();
    expect(itemManagerService.itemConfigDataSourceMap.has(testId)).toEqual(true);
  });

  it('should call sort', () => {
    const spyUpdateConfig = spyOn(itemManagerService, 'updateItemManagerConfig');
    const sort = { dir: SortDirection.asc, prop: 'title' };
    const itemConfig = { sort };
    component.onDatatableSort(sort);
    expect(spyUpdateConfig).toHaveBeenCalledWith(testId, itemConfig);
  });

  it('should call filter', () => {
    const spyUpdateConfig = spyOn(itemManagerService, 'updateItemManagerConfig');
    const filter = { title: 'a' };
    const itemConfig = { filter };
    component.onFilter(filter);
    expect(spyUpdateConfig).toHaveBeenCalledWith(testId, itemConfig);
  });

  it('should toggle favourite', () => {
    const spyUpdateConfig = spyOn(itemManagerService, 'toggleFavoriteItem');
    component.onItemSelect({});
    expect(spyUpdateConfig).toHaveBeenCalledWith({});
  });

  it('should handle scroll event', () => {
    const spyUpdateConfig = spyOn(itemManagerService, 'updateItemManagerConfig');
    component.scrolling({ target: { offsetHeight: 100, scrollTop: 0, scrollHeight: 100 } });
    expect(spyUpdateConfig).toHaveBeenCalledWith(testId, { newPage: true });
  });

  it('should handle scroll event but no scroll', () => {
    const spyUpdateConfig = spyOn(itemManagerService, 'updateItemManagerConfig');
    component.scrolling({ target: { offsetHeight: 50, scrollTop: 0, scrollHeight: 100 } });
    expect(spyUpdateConfig).not.toHaveBeenCalled();
  });
});

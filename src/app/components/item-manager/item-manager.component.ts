import { Component, HostListener, Input, OnInit } from '@angular/core';
import { SelectionType, SortPropDir } from '@swimlane/ngx-datatable';
import { uniqueId } from 'lodash';
import { Observable } from 'rxjs';
import { Item } from 'src/app/models/item.model';
import { ItemManagerService } from 'src/app/services/item-manager.service';

@Component({
  selector: 'app-item-manager',
  templateUrl: './item-manager.component.html',
  styleUrls: ['./item-manager.component.scss'],
})
export class ItemManagerComponent implements OnInit {
  @Input() visibleColumns: Observable<Array<keyof Item>> | undefined;
  @Input() visibleFilter: Map<keyof Item, boolean> | undefined;
  @Input() baseFilter: Item = {};
  @Input() selectionType: SelectionType = SelectionType.single;
  @Input() id: string = uniqueId('item_manager_');
  public rows$: Observable<Item[]> | undefined;

  constructor(private itemManagerService: ItemManagerService) {}

  @HostListener('scroll', ['$event']) scrolling(event: any): void {
    if (event.target.scrollTop + event.target.offsetHeight >= event.target.scrollHeight - 1) {
      this.itemManagerService.updateItemManagerConfig(this.id, { newPage: true });
    }
  }

  ngOnInit(): void {
    this.rows$ = this.itemManagerService.getItemList(this.id, { filter: this.baseFilter });
  }

  onDatatableSort(sort: SortPropDir): void {
    this.itemManagerService.updateItemManagerConfig(this.id, { sort });
  }

  onFilter(filter: Item): void {
    this.itemManagerService.updateItemManagerConfig(this.id, { filter });
  }

  onItemSelect(item: Item): void {
    this.itemManagerService.toggleFavoriteItem(item);
  }
}

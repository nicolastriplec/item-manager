import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ItemDatatableComponent } from '../item-datatable/item-datatable.component';
import { ItemFavoriteDialogComponent } from '../item-favorite-dialog/item-favorite-dialog.component';
import { ItemFilterComponent } from '../item-filter/item-filter.component';
import { ItemManagerComponent } from '../item-manager/item-manager.component';
import { WrapperComponent } from './wrapper.component';

describe('WrapperComponent', () => {
  let component: WrapperComponent;
  let fixture: ComponentFixture<WrapperComponent>;
  const dialogRefSpyObj = jasmine.createSpyObj({ close: null, componentInstance: { body: '' } });
  let dialogSpy: jasmine.Spy;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WrapperComponent, ItemManagerComponent, ItemFilterComponent, ItemDatatableComponent],
      imports: [
        HttpClientModule,
        MatDialogModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatButtonModule,
        MatInputModule,
        BrowserAnimationsModule,
        NgxDatatableModule,
        MatIconModule,
        MatBadgeModule,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    dialogSpy = spyOn(TestBed.inject(MatDialog), 'open').and.returnValue(dialogRefSpyObj);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open dialog', async () => {
    component.openFavoriteItemsDialog();
    expect(dialogSpy).toHaveBeenCalledWith(ItemFavoriteDialogComponent, { panelClass: 'dialog-no-border' });
  });
});

import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable, of } from 'rxjs';
import { Item } from 'src/app/models/item.model';
import { ItemManagerService } from 'src/app/services/item-manager.service';
import { ItemFavoriteDialogComponent } from '../item-favorite-dialog/item-favorite-dialog.component';

@Component({
  selector: 'app-wrapper',
  templateUrl: './wrapper.component.html',
  styleUrls: ['./wrapper.component.scss'],
})
export class WrapperComponent {
  public visibleColumns: Observable<Array<keyof Item>> = of(['favorite', 'title', 'description', 'price', 'email', 'image']);
  constructor(public itemManagerService: ItemManagerService, private dialog: MatDialog) {}

  openFavoriteItemsDialog(): void {
    this.dialog.open(ItemFavoriteDialogComponent, { panelClass: 'dialog-no-border' });
  }
}

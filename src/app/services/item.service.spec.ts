import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { SortDirection } from '@swimlane/ngx-datatable';
import { get, intersection } from 'lodash';
import { environment } from 'src/environments/environment';
import { itemsMock } from '../mocks/items-mock';
import { ItemManagerConfig } from '../models/item-manager-config';
import { CompareUtils } from '../utils/compare.utils';
import { ItemService, PAGE_SIZE } from './item.service';

describe('ItemService', () => {
  let service: ItemService;
  let httpMock: HttpTestingController;
  let http: HttpClient;
  const itemManagerConfig: ItemManagerConfig = {};

  beforeEach(() => {
    TestBed.configureTestingModule({ imports: [HttpClientTestingModule] });
    service = TestBed.inject(ItemService);
    httpMock = TestBed.inject(HttpTestingController);
    http = TestBed.inject(HttpClient);
    service.itemList = itemsMock;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('has compareUtils', () => {
    expect(CompareUtils.getSortDirection(SortDirection.asc)).toEqual(1);
    expect(CompareUtils.getSortDirection(SortDirection.desc)).toEqual(-1);
    expect(CompareUtils.isNumeric(100)).toEqual(false);
    expect(CompareUtils.isNumeric('100')).toEqual(true);
  });

  it('should request items', done => {
    service.itemList = [];
    service.requestItems(itemManagerConfig).subscribe((config: ItemManagerConfig) => {
      for (let i = 0; i < get(config, `currentItems.length`, 0); i++) {
        expect(get(config, `currentItems[${i}].title`, 0)).toEqual(get(itemsMock, `[${i}].title`, 0));
      }
      done();
    });
    const call = httpMock.expectOne(`${environment.baseUrl}/items.json`);
    expect(call.request.method).toEqual('GET');
    call.flush({ items: itemsMock });
  });

  it('should get cached items', done => {
    service.requestItems(itemManagerConfig).subscribe((config: ItemManagerConfig) => {
      expect(config).toEqual({ currentItems: itemsMock });
      done();
    });
    httpMock.expectNone(`${environment.baseUrl}/items.json`);
  });

  it('should get items page', () => {
    const firstPage = service.getItemsPage({ currentItems: itemsMock });
    expect(firstPage.currentItemsLength).toEqual(PAGE_SIZE);
    expect(firstPage.currentItems).toHaveSize(PAGE_SIZE);
    const firstPageAgain = service.getItemsPage({ ...firstPage, currentItems: itemsMock });
    expect(firstPageAgain).toEqual(firstPage);
    const secondPage = service.getItemsPage({ ...firstPage, currentItems: itemsMock, newPage: true });
    expect(secondPage.currentItemsLength).toEqual(Math.min(PAGE_SIZE * 2, itemsMock.length));
    expect(secondPage.currentItems).toHaveSize(Math.min(PAGE_SIZE * 2, itemsMock.length));
  });

  it('should toggle favorite items', () => {
    service.toggleFavoriteItem(itemsMock[0]);
    expect(service.itemList[0].favorite).toEqual(true);
  });

  it('should filter items', () => {
    const noFilter = service.filterItems({ currentItems: itemsMock });
    expect(noFilter.currentItems).toHaveSize(itemsMock.length);
    const [titleFilter, priceFilter, descriptionFilter, emailFilter] = [{ title: 'P' }, { price: '100' }, { description: 'y' }, { email: 'a@' }];
    const filterByTitle = service.filterItems({ ...noFilter, filter: titleFilter });
    expect(filterByTitle.currentItems).toHaveSize(5);
    const filterByPrice = service.filterItems({ ...noFilter, filter: priceFilter });
    expect(filterByPrice.currentItems).toHaveSize(4);
    const filterByDescription = service.filterItems({ ...noFilter, filter: descriptionFilter });
    expect(filterByDescription.currentItems).toHaveSize(7);
    const filterByEmail = service.filterItems({ ...noFilter, filter: emailFilter });
    expect(filterByEmail.currentItems).toHaveSize(2);
    const filterAll = service.filterItems({ ...noFilter, filter: { ...emailFilter, ...titleFilter, ...priceFilter, ...descriptionFilter } });
    expect(filterAll.currentItems).toEqual(intersection(filterByTitle.currentItems, filterByPrice.currentItems, filterByDescription.currentItems, filterByEmail.currentItems));
  });

  it('should sort items', () => {
    const noSort = service.sortItems({ currentItems: itemsMock });
    expect(get(noSort, `currentItems[0].title`)).toEqual(get(itemsMock, `[0].title`));
    const sortByTitle = service.sortItems({ currentItems: itemsMock, sort: { dir: SortDirection.asc, prop: 'title' } });
    expect(get(sortByTitle, `currentItems[0].title`)).toEqual('Barbacoa');
    const sortByPrice = service.sortItems({ currentItems: itemsMock, sort: { dir: SortDirection.asc, prop: 'price' } });
    expect(get(sortByPrice, `currentItems[0].title`)).toEqual('Coche antiguo americano');
  });
});

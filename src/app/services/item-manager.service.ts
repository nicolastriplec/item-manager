import { Injectable } from '@angular/core';
import { merge, sumBy } from 'lodash';
import { BehaviorSubject, combineLatest, Observable, of } from 'rxjs';
import { finalize, map, share, switchMap, tap } from 'rxjs/operators';
import { ItemManagerConfig, ItemManagerConfigDataSource } from '../models/item-manager-config';
import { Item } from '../models/item.model';
import { ItemService } from './item.service';

@Injectable({
  providedIn: 'root',
})
export class ItemManagerService {
  private favoriteCount = new BehaviorSubject(0);
  private refresh$ = new BehaviorSubject(true);
  public itemConfigDataSourceMap: Map<string, ItemManagerConfigDataSource> = new Map();
  constructor(private itemService: ItemService) {}

  getItemList(id: string, itemManagerBaseConfig?: ItemManagerConfig): Observable<Item[]> {
    if (!this.itemConfigDataSourceMap.has(id)) {
      const update = new BehaviorSubject(itemManagerBaseConfig || {});
      const data: Observable<Item[]> = combineLatest([update, this.refresh$]).pipe(
        switchMap(([itemManagerConfig]) => this.itemService.requestItems(itemManagerConfig)),
        tap(itemManagerConfig => this.favoriteCount.next(sumBy(itemManagerConfig.currentItems, row => (row.favorite ? 1 : 0)))),
        map(itemManagerConfig => this.itemService.filterItems(itemManagerConfig)),
        map(itemManagerConfig => this.itemService.sortItems(itemManagerConfig)),
        map(itemManagerConfig => this.itemService.getItemsPage(itemManagerConfig)),
        tap(itemManagerConfig => this.itemConfigDataSourceMap.set(id, { ...this.itemConfigDataSourceMap.get(id), itemManagerConfig })),
        map(itemManagerConfig => itemManagerConfig.currentItems!),
        finalize(() => this.itemConfigDataSourceMap.delete(id)),
        share()
      );
      this.itemConfigDataSourceMap.set(id, { update, data, itemManagerBaseConfig });
      return data;
    } else {
      return this.itemConfigDataSourceMap.get(id)!.data!;
    }
  }

  updateItemManagerConfig(id: string, newItemManagerConfig: ItemManagerConfig): void {
    if (this.itemConfigDataSourceMap.has(id) && newItemManagerConfig) {
      const itemManagerConfigDataSource = this.itemConfigDataSourceMap.get(id);
      itemManagerConfigDataSource!.update!.next(
        merge({ ...itemManagerConfigDataSource!.itemManagerConfig, ...newItemManagerConfig }, itemManagerConfigDataSource!.itemManagerBaseConfig)
      );
    }
  }

  toggleFavoriteItem(favItem: Item): void {
    this.itemService.toggleFavoriteItem(favItem);
    this.refresh$.next(true);
  }

  getFavoriteCount(): Observable<number> {
    return this.favoriteCount.asObservable();
  }
}

import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { combineLatest, merge, of, Subject } from 'rxjs';
import { delay, finalize, map, take, takeUntil, tap } from 'rxjs/operators';
import { TestScheduler } from 'rxjs/testing';
import { itemsMock } from '../mocks/items-mock';

import { ItemManagerService } from './item-manager.service';
import { ItemService } from './item.service';

describe('ItemManagerService', () => {
  let service: ItemManagerService;
  let itemService: ItemService;
  let scheduler: TestScheduler;
  const itemManagerId = 'testId';
  const itemConfig = { currentItems: itemsMock };

  beforeEach(() => {
    TestBed.configureTestingModule({ imports: [HttpClientModule] });
    service = TestBed.inject(ItemManagerService);
    itemService = TestBed.inject(ItemService);
    spyOn(itemService, 'getItemsPage').and.returnValue(itemConfig);
    spyOn(itemService, 'filterItems').and.returnValue(itemConfig);
    spyOn(itemService, 'sortItems').and.returnValue(itemConfig);
    scheduler = new TestScheduler((actual, expected) => {
      expect(actual).toEqual(expected);
    });
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should create new itemConfigDataSource', done => {
    spyOn(itemService, 'requestItems').and.returnValue(of(itemConfig));
    service
      .getItemList(itemManagerId)
      .pipe(
        finalize(() =>
          of(null)
            .pipe(delay(200))
            .subscribe(_ => (expect(service.itemConfigDataSourceMap.size).toEqual(0), done()))
        )
      )
      .subscribe(rows => {
        expect(rows).toEqual(itemsMock);
        expect(service.itemConfigDataSourceMap.size).toEqual(1);
      })
      .unsubscribe();
  });

  it('should get current itemConfigDataSource', done => {
    spyOn(itemService, 'requestItems').and.returnValue(of(itemConfig));
    const unsubscribe$ = new Subject();
    service.getItemList(itemManagerId).pipe(takeUntil(unsubscribe$)).subscribe();
    service
      .getItemList(itemManagerId)
      .pipe(
        takeUntil(unsubscribe$),
        finalize(() => done())
      )
      .subscribe(_ => {
        expect(service.itemConfigDataSourceMap.size).toEqual(1);
        done();
      });
    unsubscribe$.next();
    unsubscribe$.complete();
  });

  it('should update current itemConfigDataSource', () => {
    spyOn(itemService, 'requestItems').and.returnValue(of({ ...itemConfig, currentItems: itemsMock.map(item => ({ ...item, favorite: true })) }));
    scheduler.run(({ expectObservable }) => {
      const rows$ = combineLatest([
        service.getItemList(itemManagerId),
        merge(
          of(undefined),
          of(undefined).pipe(
            delay(2),
            tap(() => service.toggleFavoriteItem({}))
          )
        ),
      ]).pipe(
        map(([items]) => items),
        take(2)
      );
      const itemsMarble = '(a(a|))';
      const expectedItems = { a: itemsMock };
      expectObservable(rows$).toBe(itemsMarble, expectedItems);
    });
  });

  it('should toggle favorite items and refresh all itemConfigDataSource', () => {
    spyOn(itemService, 'requestItems').and.returnValue(of(itemConfig));
    scheduler.run(({ expectObservable }) => {
      const rows$ = combineLatest([
        service.getItemList(itemManagerId),
        merge(
          of(undefined),
          of(undefined).pipe(
            delay(2),
            tap(() => service.updateItemManagerConfig(itemManagerId, {}))
          )
        ),
      ]).pipe(
        map(([items]) => items),
        take(2)
      );
      const itemsMarble = '(a(a|))';
      const expectedItems = { a: itemsMock };
      expectObservable(rows$).toBe(itemsMarble, expectedItems);
    });
  });
});

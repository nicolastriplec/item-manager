import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { every, get, isEmpty, toPairs, uniqueId } from 'lodash';
import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ItemManagerConfig } from '../models/item-manager-config';
import { Item, ItemResponse } from '../models/item.model';
import { CompareUtils } from '../utils/compare.utils';

export const PAGE_SIZE = 5;

@Injectable({
  providedIn: 'root',
})
export class ItemService {
  public itemList: Array<Item> = [];
  constructor(private httpClient: HttpClient) {}

  requestItems(itemManagerConfig: ItemManagerConfig): Observable<ItemManagerConfig> {
    if (!isEmpty(this.itemList)) {
      return of({ ...itemManagerConfig, currentItems: this.itemList });
    }
    return this.httpClient.get<ItemResponse>(`${environment.baseUrl}/items.json`).pipe(
      map(itemResponse => (itemResponse.items || []).map(item => ({ ...item, id: uniqueId('item_'), favorite: false }))),
      tap(itemList => (this.itemList = itemList)),
      map(responseItemList => ({ ...itemManagerConfig, currentItems: responseItemList }))
    );
  }

  getItemsPage(itemManagerConfig: ItemManagerConfig): ItemManagerConfig {
    const currentItemListLength = Math.min(this.itemList.length, Math.max(itemManagerConfig.currentItemsLength || 0, PAGE_SIZE) + ((itemManagerConfig.newPage && PAGE_SIZE) || 0));
    const currentItems = itemManagerConfig.currentItems?.slice(0, currentItemListLength);
    return { ...itemManagerConfig, currentItems, currentItemsLength: (currentItems || []).length };
  }

  toggleFavoriteItem(favItem: Item): void {
    this.itemList = this.itemList.map(item => (item.id === favItem.id ? { ...item, favorite: !item.favorite } : item));
  }

  filterItems(itemManagerConfig: ItemManagerConfig): ItemManagerConfig {
    if (isEmpty(itemManagerConfig.filter)) {
      return itemManagerConfig;
    }
    return {
      ...itemManagerConfig,
      currentItems: itemManagerConfig.currentItems?.filter(matchItem =>
        every(toPairs(matchItem), ([key, value]) => {
          const filterValue = get(itemManagerConfig.filter, `${key}`, '');
          if (CompareUtils.isNumeric(value)) {
            return !filterValue || (value && parseFloat(value) < parseFloat(filterValue));
          } else if (typeof get(itemManagerConfig.filter, `${key}`) === 'boolean') {
            return value === get(itemManagerConfig.filter, `${key}`);
          } else {
            return !`${filterValue || ''}`.trim() || (`${value}`.trim() && `${value}`.trim().toLowerCase().includes(`${filterValue}`.trim().toLowerCase()));
          }
        })
      ),
    };
  }

  sortItems(itemManagerConfig: ItemManagerConfig): ItemManagerConfig {
    if (isEmpty(itemManagerConfig.sort)) {
      return itemManagerConfig;
    }
    const { prop, dir } = itemManagerConfig.sort!;
    return {
      ...itemManagerConfig,
      currentItems: itemManagerConfig.currentItems?.sort((itemA, itemB) => this.compareItems(itemA, itemB, prop as keyof Item) * CompareUtils.getSortDirection(dir)),
    };
  }

  compareItems(itemA: Item, itemB: Item, prop: keyof Item): number {
    if (!CompareUtils.isNumeric(get(itemA, `${prop}`))) {
      return get(itemA, `${prop}`, '').localeCompare(get(itemB, `${prop}`, ''));
    }
    return get(itemB, `${prop}`, 0) - get(itemA, `${prop}`, 0);
  }
}

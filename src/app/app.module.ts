import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import {
  MatFormFieldModule,
  MAT_FORM_FIELD_DEFAULT_OPTIONS,
} from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ItemDatatableComponent } from './components/item-datatable/item-datatable.component';
import { ItemFavoriteDialogComponent } from './components/item-favorite-dialog/item-favorite-dialog.component';
import { ItemFilterComponent } from './components/item-filter/item-filter.component';
import { ItemManagerComponent } from './components/item-manager/item-manager.component';
import { WrapperComponent } from './components/wrapper/wrapper.component';

@NgModule({
  declarations: [
    AppComponent,
    ItemManagerComponent,
    ItemDatatableComponent,
    ItemFilterComponent,
    WrapperComponent,
    ItemFavoriteDialogComponent,
  ],
  imports: [
    ReactiveFormsModule,
    MatButtonModule,
    MatIconModule,
    FormsModule,
    MatFormFieldModule,
    MatBadgeModule,
    BrowserModule,
    MatButtonModule,
    MatInputModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgxDatatableModule,
    HttpClientModule,
    MatDialogModule,
    RouterModule.forRoot([{ path: '**', component: WrapperComponent }]),
  ],
  bootstrap: [AppComponent],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: { appearance: 'outline' },
    },
  ],
})
export class AppModule {}

export interface Item {
  title?: string;
  description?: string;
  price?: string;
  email?: string;
  image?: string;
  id?: string;
  favorite?: boolean;
  remove?: boolean;
}

export interface ItemResponse {
  items: Array<Item>;
}

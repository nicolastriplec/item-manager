import { SortPropDir } from '@swimlane/ngx-datatable';
import { BehaviorSubject, Observable } from 'rxjs';
import { Item } from './item.model';

export interface ItemManagerConfig {
  currentItemsLength?: number;
  newPage?: boolean;
  sort?: SortPropDir;
  filter?: Item;
  currentItems?: Item[];
}

export interface ItemManagerConfigDataSource {
  update?: BehaviorSubject<ItemManagerConfig>;
  itemManagerConfig?: ItemManagerConfig;
  itemManagerBaseConfig?: ItemManagerConfig;
  data?: Observable<Item[]>;
}

import { SortDirection, SortPropDir, TableColumn } from '@swimlane/ngx-datatable';

export interface SortingEvent {
  column: TableColumn;
  newValue: SortDirection;
  prevValue: SortDirection;
  sorts: Array<SortPropDir>;
}
